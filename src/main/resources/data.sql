INSERT INTO oauth_users (id, username, password, email, first_name, last_name)
VALUES (1, 'user@gmail.com', '$2a$10$Q3PPE605LxYb51.sO4G.3OMAF2WjAvkfr8tuoR6MZP5EgBinzFFfy', 'user@gmail.com', 'Nidhish', 'Krishnan');

INSERT INTO oauth_users (id, username, password, email, first_name, last_name)
VALUES (2, 'admin@gmail.com', '$2a$10$rJknmXG57lPul2Sa5fQcUu5YbWeYuG8LvA7vWckWXjQLt0RdJx.X2', 'admin@gmail.com', 'Nidhish', 'Krishnan');


INSERT INTO oauth_roles (id, name) VALUES (1,'ROLE_USER');
INSERT INTO oauth_roles (id, name) VALUES (2,'ROLE_ADMIN');

INSERT INTO oauth_user_role_mapping (user_id, role_id) VALUES (1,1);
INSERT INTO oauth_user_role_mapping (user_id, role_id) VALUES (2,1);
INSERT INTO oauth_user_role_mapping (user_id, role_id) VALUES (2,2);

CREATE TABLE oauth_client_details (
  client_id VARCHAR(256) PRIMARY KEY,
  resource_ids VARCHAR(256),
  client_secret VARCHAR(256),
  scope VARCHAR(256),
  authorized_grant_types VARCHAR(256),
  web_server_redirect_uri VARCHAR(256),
  authorities VARCHAR(256),
  access_token_validity INTEGER,
  refresh_token_validity INTEGER,
  additional_information VARCHAR(4096),
  autoapprove VARCHAR(256)
);

INSERT INTO oauth_client_details (client_id, client_secret, scope, authorized_grant_types, authorities) VALUES
('22e14922-87d7-4ee4-a470-da0bb10d45d3', '$2a$10$Is6r80wW65hKHUq6Wa8B6O3BLKqGOb5McDGbJUwVwfVvyeJBCf7ta', 'resource-access', 'implicit,authorization_code,refresh_token,password,client_credentials', 'ROLE_CLIENT,ROLE_USER')



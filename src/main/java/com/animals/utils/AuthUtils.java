package com.animals.utils;

import com.animals.model.Role;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public class AuthUtils {
    /**
	 * utility method to convert the user roles to a Collection<GrantedAuthority> for spring security to deal with 
	 * @param roles the list of roles as string
	 * @return a collection of SimpleGrantedAuthority that represent user roles
	 */
	public static Collection<SimpleGrantedAuthority> getUserAuthorities(Set<Role> roles) {
		return roles.stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toSet());
	}
}

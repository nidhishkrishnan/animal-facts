package com.animals.service;

import com.animals.domain.CatFacts;
import com.animals.gateway.CatGateway;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class CatService {

    private final CatGateway catFactsGateway;

    public CatFacts getCatFacts() {
        return catFactsGateway.getCatFacts();
    }
}


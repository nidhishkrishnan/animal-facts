package com.animals.service;

import com.animals.common.Constants;
import com.animals.domain.UserCredentials;
import com.google.common.collect.Maps;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.stereotype.Service;
import org.springframework.web.HttpRequestMethodNotSupportedException;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import static java.util.Collections.emptyList;
import static org.springframework.security.oauth2.common.OAuth2AccessToken.REFRESH_TOKEN;
import static org.springframework.security.oauth2.common.util.OAuth2Utils.GRANT_TYPE;
import static com.animals.config.AppConfig.AuthProperties;

@Slf4j
@Service
@RequiredArgsConstructor
public class ApiService implements Constants {

	private final TokenEndpoint tokenEndpoint;

	private final AuthProperties authProperties;

	public ResponseEntity login(final UserCredentials credentials, HttpServletResponse servletResponse) {
		Map<String, String> parameters = Maps.newHashMap();
		parameters.put(USERNAME, credentials.getUsername());
		parameters.put(PASSWORD, credentials.getPassword());
		parameters.put(GRANT_TYPE, PASSWORD);
		return getPartValue(
				parameters,
				servletResponse,
				ex -> log.error("Exception in loginProxy: {}", ex.getMessage())
		);
	}

	public ResponseEntity refreshToken(final Map<String,String> parameters) {
		parameters.put(GRANT_TYPE, REFRESH_TOKEN);
		return getPartValue(
				parameters,
				null,
				ex -> log.error("Exception in refreshToken: {}", ex.getMessage())
		);
	}

	private ResponseEntity<?> getPartValue(final Map<String,String> parameters, final HttpServletResponse servletResponse, final Consumer<Exception> onException) {
		try {
			return generateAccessTokens(parameters, authProperties.getClientId());
		} catch (Exception exception) {
			onException.accept(exception);
			return ResponseEntity
				.status(org.springframework.http.HttpStatus.FORBIDDEN)
				.body(response(org.springframework.http.HttpStatus.FORBIDDEN, exception.getMessage()));
		}
	}

	private ResponseEntity<OAuth2AccessToken> generateAccessTokens(Map<String,String> parameters, String clientId) throws HttpRequestMethodNotSupportedException {
		return tokenEndpoint.postAccessToken(new UsernamePasswordAuthenticationToken(clientId, authProperties.getClientSecret(), emptyList()), parameters);
	}

	private Map<String, Object> response(org.springframework.http.HttpStatus status, String message) {
		Map<String, Object> response = new HashMap<>();
		response.put(STATUS, status.value());
		response.put(MESSAGE, message);
		return response;
	}
}

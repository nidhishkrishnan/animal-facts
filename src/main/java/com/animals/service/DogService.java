package com.animals.service;

import com.animals.gateway.DogGateway;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@Service
@RequiredArgsConstructor
public class DogService {

    private final DogGateway dogGateway;

    @Cacheable("dogImages")
    public List<String> getDogImages() {
        return IntStream.range(0, 250)
                .mapToObj(i -> dogGateway.getDogImages())
                .filter(StringUtils::isNoneBlank)
                .collect(Collectors.toList());
    }
}

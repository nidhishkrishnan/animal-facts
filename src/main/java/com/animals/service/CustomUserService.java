package com.animals.service;

import com.animals.model.User;
import com.animals.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomUserService {

	private final UserRepository userRepository;

	public Optional<User> findByUserId(final String username) {
		return userRepository.findByUsername(username);
	}
}

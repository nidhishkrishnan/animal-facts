package com.animals.service;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static com.animals.utils.AuthUtils.getUserAuthorities;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

  private final CustomUserService userService;

  @Override
  public UserDetails loadUserByUsername(String username)  {
    if (username == null || username.isEmpty()) {
      throw new UsernameNotFoundException("Username is empty");
    }
    com.animals.model.User user = userService.findByUserId(username).orElseThrow(() -> new UsernameNotFoundException("Unauthorized client_id or username not found: " + username));
    return new User(user.getUsername(), StringUtils.EMPTY, getUserAuthorities(user.getRoles()));
  }
}

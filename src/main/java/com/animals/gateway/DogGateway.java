package com.animals.gateway;

import com.animals.client.DogClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class DogGateway {

    private final DogClient dogClient;

    public String getDogImages() {
        return dogClient.getDogImages().getMessage();
    }
}

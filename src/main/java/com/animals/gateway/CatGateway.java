package com.animals.gateway;

import com.animals.client.CatFactsClient;
import com.animals.domain.CatFacts;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class CatGateway {

    private final CatFactsClient catFactsClient;

    public CatFacts getCatFacts() {
        return catFactsClient.getCatFacts();
    }
}

package com.animals.config;

import com.animals.common.Constants;
import com.animals.model.User;
import com.animals.service.CustomUserDetailsService;
import com.animals.service.CustomUserService;
import com.google.common.collect.Maps;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.approval.ApprovalStore;
import org.springframework.security.oauth2.provider.approval.JdbcApprovalStore;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

import javax.sql.DataSource;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Configuration
@RequiredArgsConstructor
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    private final DataSource dataSource;

    private final CustomUserDetailsService userDetailsService;

    private final AuthenticationManager authenticationManagerBean;

    private final CustomUserService userService;

    private final PasswordEncoder passwordEncoder;

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(jwtAccessTokenConverter());
    }

    @Bean
    public ApprovalStore approvalStore() {
        return new JdbcApprovalStore(dataSource);
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.jdbc(dataSource).passwordEncoder(passwordEncoder);
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer) {
    	oauthServer.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints
            .tokenStore(tokenStore())
            .tokenEnhancer(jwtAccessTokenConverter())
            .authenticationManager(authenticationManagerBean)
            .userDetailsService(userDetailsService);
    }

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter converter = new CustomTokenEnhancer();
        converter.setKeyPair(new KeyStoreKeyFactory(new ClassPathResource("oauth-jwt.jks"), "try@123456".toCharArray()).getKeyPair("oauth-jwt"));
        return converter;
    }

    /*
     * Add custom user principal information to the JWT token
     */
    private class CustomTokenEnhancer extends  JwtAccessTokenConverter  {

        @Override
        public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
            Optional<User> user = userService.findByUserId(authentication.getName().toLowerCase());
            DefaultOAuth2AccessToken customAccessToken = new DefaultOAuth2AccessToken(accessToken);
            if (user.isPresent()) {
                Map<String, Object> additionalInformation = new LinkedHashMap<>(accessToken.getAdditionalInformation());
                additionalInformation.put(Constants.USER_INFO, buildUserDetails(user.get(), authentication.getAuthorities()));
                customAccessToken.setAdditionalInformation(additionalInformation);
                return super.enhance(customAccessToken, authentication);
            }
            return super.enhance(customAccessToken, authentication);
        }

        private Map<String, Object> buildUserDetails(final User user, Collection<GrantedAuthority> authorities) {
            Map<String, Object> userDetails = Maps.newHashMap();
            userDetails.put(Constants.USERNAME, user.getUsername());
            userDetails.put(Constants.FIRST_NAME, user.getFirstName());
            userDetails.put(Constants.LAST_NAME, user.getLastName());
            userDetails.put(Constants.EMAIL, user.getEmail());
            userDetails.put(Constants.USER_ROLES, authorities.stream().map(r -> r.getAuthority()).collect(Collectors.toList()));
            return userDetails;
        }
    }
}

package com.animals.config;

import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import static com.animals.config.AppConfig.AuthProperties;

@Configuration
@EnableConfigurationProperties({ AuthProperties.class })
public class AppConfig {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Data
    @ToString
    @ConfigurationProperties("auth")
    public static class AuthProperties {
        private String clientId;
        private String clientSecret;
    }
}

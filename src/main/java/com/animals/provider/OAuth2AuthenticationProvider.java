package com.animals.provider;

import com.animals.model.User;
import com.animals.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static com.animals.utils.AuthUtils.getUserAuthorities;

@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class OAuth2AuthenticationProvider implements AuthenticationProvider {

	private final UserRepository userRepository;

	private final PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(final Authentication authentication) {
    	String username = ((String) authentication.getPrincipal()).toLowerCase();
    	String password = (String) authentication.getCredentials();
		if (username != null && authentication.getCredentials() != null) {
			User user = invokeAuthentication(username, password).orElse(new User());
			return new UsernamePasswordAuthenticationToken(username, StringUtils.EMPTY, getUserAuthorities(user.getRoles()));
		 } else {
			 throw new BadCredentialsException("Username or Password cannot be empty!!!");
	     }
    }

	public Optional<User> invokeAuthentication(String username, String password) {
		Optional<User> user = userRepository.findByUsername(username);
		if(user.isPresent() && passwordEncoder.matches(password, user.get().getPassword())) {
			return user;
		}
		throw new BadCredentialsException("Authentication Failed!!!");
	}

    @Override
    public boolean supports(Class<?> authentication) {
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }
}

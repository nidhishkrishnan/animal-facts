package com.animals.client;


import com.animals.domain.DogApiResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "dog-service", url="https://dog.ceo/api")
public interface DogClient {

    @GetMapping("/breeds/image/random")
    DogApiResponse getDogImages();
}

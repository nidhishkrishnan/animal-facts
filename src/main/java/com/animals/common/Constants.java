package com.animals.common;

public interface Constants {

	String PASSWORD			= "password";
	String MESSAGE			= "message";
	String STATUS			= "status";
    String USER_INFO        = "userInfo";
    String USERNAME         = "username";
    String FIRST_NAME       = "firstName";
    String LAST_NAME        = "lastName";
    String USER_ROLES       = "userRoles";
    String EMAIL            = "email";
}

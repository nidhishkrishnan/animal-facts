package com.animals.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class CatFacts {
    /*{
        "status": {
        "verified": true,
        "sentCount": 1
        },


        "type": "dog",
        "deleted": false,
        "_id": "5b7a03082048fd0014e9d87f",
        "updatedAt": "2020-08-23T20:20:01.611Z",
        "createdAt": "2018-08-19T23:53:44.704Z",
        "user": "5a9ac18c7478810ea6c06381",
        "text": "Nearly all but two breeds of dogs have pink tongues: the Chow Chow and the Shar-pei both have black tongues.",
        "source": "user",
        "__v": 0,
        "used": false
        }*/
    private String type;

    private Boolean deleted;

    private LocalDateTime updatedAt;

    private LocalDateTime createdAt;

    private String text;

    private String source;

    @JsonProperty("_id")
    private String id;

    @JsonProperty("__v")
    private Integer version;
}

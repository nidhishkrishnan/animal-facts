package com.animals;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableCaching
@EnableSwagger2
@EnableFeignClients
@SpringBootApplication
public class AnimalFactsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnimalFactsApplication.class, args);
	}
}

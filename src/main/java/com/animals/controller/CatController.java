package com.animals.controller;

import com.animals.domain.CatFacts;
import com.animals.service.CatService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/cat/")
@RequiredArgsConstructor
public class CatController {

    private final CatService catService;

    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Enter Access Token", defaultValue = "Bearer YOUR_TOKEN",
                    required = true, dataType = "string", paramType = "header")})
    @GetMapping("facts")
    public CatFacts getCatFact() {
        return catService.getCatFacts();
    }
}

package com.animals.controller;

import com.animals.service.DogService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("v1/dog/")
@RequiredArgsConstructor
public class DogController {

    private final DogService dogService;

    @GetMapping("images")
    public List<String> getDogImages() {
        return dogService.getDogImages();
    }
}

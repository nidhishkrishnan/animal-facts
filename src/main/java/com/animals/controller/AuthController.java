package com.animals.controller;

import com.animals.domain.UserCredentials;
import com.animals.service.ApiService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class AuthController {

	private final ApiService apiService;

	@ApiOperation(httpMethod = "POST", value = "Refresh and get new Access Tokens", response = Map.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@PostMapping("/user/refresh")
	public ResponseEntity refresh(@ApiParam(value = "Provide valid refresh_token", required = true) @RequestBody final Map<String,String> tokenDetails) {
		return apiService.refreshToken(tokenDetails);
	}

	@ApiOperation(httpMethod = "POST", value = "Login to Auth Server", response = Map.class, produces = MediaType.APPLICATION_JSON_VALUE, authorizations = {@Authorization(value="basicAuth")})
	@PostMapping("/user/login")
	public ResponseEntity login(@ApiParam(value = "provide valid username and password details", required = true) @RequestBody final UserCredentials credentials, HttpServletResponse httpServletResponse) {
		return apiService.login(credentials, httpServletResponse);
	}
}

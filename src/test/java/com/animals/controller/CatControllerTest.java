package com.animals.controller;

import com.animals.domain.CatFacts;
import com.animals.service.CatService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@DisplayName("Testing Cat Controller")
class CatControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private CatController catController;

    @Mock
    private CatService catService;

    @BeforeEach
    void setMockOutput()
    {
        mockMvc = MockMvcBuilders.standaloneSetup(catController).build();
    }

    @Test
    @DisplayName("Test for getting Cat fact ('v1/cat/facts')")
    void getCatFactsTest() throws Exception
    {
        when(catService.getCatFacts()).thenReturn(buildCatFacts());
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/cat/facts/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    private CatFacts buildCatFacts() {
        CatFacts catFacts = new CatFacts();
        catFacts.setText("Some Cat Facts");
        return catFacts;
    }

}

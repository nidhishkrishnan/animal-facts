package com.animals.service;

import com.animals.domain.CatFacts;
import com.animals.gateway.CatGateway;
import com.animals.gateway.DogGateway;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@DisplayName("Testing Cat Service")
class CatServiceTest {

    @Mock
    private CatGateway catGateway;

    @InjectMocks
    private CatService catService;

    @Test
    @DisplayName("Test for getting Cat Facts from public cat service")
    void getCatFactsTest() {
        when(catGateway.getCatFacts()).thenReturn(buildCatFacts());
        CatFacts catFacts = catService.getCatFacts();
        assertEquals("Some Cats Fact", catFacts.getText());
    }

    private CatFacts buildCatFacts() {
        CatFacts catFacts = new CatFacts();
        catFacts.setText("Some Cats Fact");
        return catFacts;
    }

}

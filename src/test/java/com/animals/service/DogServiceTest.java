package com.animals.service;

import com.animals.gateway.DogGateway;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@DisplayName("Testing Dog Service")
class DogServiceTest {

    @Mock
    private DogGateway dogGateway;

    @InjectMocks
    private DogService dogService;

    @Test
    @DisplayName("Test for getting Dog images from public dog service")
    void getDogImagesTest() {
        String expectedImage = "https://images.dog.ceo/breeds/retriever-golden/leo_small.jpg";
        when(dogGateway.getDogImages()).thenReturn(expectedImage);
        List<String> images = dogService.getDogImages();
        assertEquals(expectedImage, images.get(1));
    }
}
